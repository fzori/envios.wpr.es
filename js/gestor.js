var 	loaded_file ='';
var		bottom_page = 1;
var		top_page = 10;
var 	upload_url = "";
var 	minutos = "";
var 	envios_min = "";

$(document).ready(function()
{
	if( $('#logo').val() == "")
	{
		 $('.logo').remove();
	}
  	$('.subir').hide(); // hay que ocultarlo
	credito();
	$(function() 
	{
	//	$("#fechaenvio").datetimepicker({
	//	dateFormat: 'dd/mm/yy HH:mm'
	//	});



$('#datetimepicker1').datetimepicker({
	pickSeconds: false,
    });
$(".bootstrap-timepicker").datetimepicker({
    pickDate: false,
	pickSeconds: false,
    })

	$(".bootstrap-timepicker").on('show',function (e)
	{
    });

	$('#manual').click(function() 
    {
    	var		form = $("<form id='desc_manual' action='scripts/download_manual.php'></form>");
    	form.appendTo( $('#manual') );
    	$('#desc_manual').submit();
    	$('#desc_manual').remove();
    });
    // opciones avanzadas check box ipl
    $('#chk_distribuido').click(function() 
    {
    	removeError( $('#chk_distribuido') );
    	removeError( $('#chk_por_minuto') );

        if($(this).is(":checked")) 
        {
        	$('#distribuirenvio').removeAttr( 'disabled' );
        	//desmarcamos el otro checkbox y desabilitamos la caja de texto
        	$('#chk_por_minuto').attr('checked', false);
        	$('#distribuirenvioporminuto').attr( 'disabled','disabled' );


 		} 
 		else
 		{
 			$('#distribuirenvio').val( '' );
			//$('#distribuirenvio').attr( 'disabled','disabled' );

 		}// end of if
    });
    // funcionalidad del segundo checkbox
        $('#chk_por_minuto').click(function() {
        	removeError( $('#chk_distribuido') );
    		removeError( $('#chk_por_minuto') );
        
        	if($(this).is(":checked")) 
       		{
        		$('#distribuirenvioporminuto').removeAttr( 'disabled' );
        		//desmarcamos el otro checkbox y desabilitamos la caja de texto
        		$('#chk_distribuido').attr('checked', false);
        		$('#distribuirenvio').attr( 'disabled','disabled' );
 			} 
 			else
 			{
 				$('#distribuirenvioporminuto').val( '' );
				$('#distribuirenvioporminuto').attr( 'disabled','disabled' );
 			}// end of if
    	});

        // cierre dialogo info
        $('#close_info').click(function()
       {
       		$('#info_label').html(  )
       		$('#INFO').hide();

       });
       $('#close_error').click(function()
       {
       		$('#error_label').html(  )
       		$('#ERRORINFO').hide();

       });
        $('#error_aceptar').click(function()
       {
       		$('#error_label').html(  )
       		$('#ERRORINFO').hide();

       });
       // cierre gestor ficheros
       $('#close_subir').click(function()
       {
       		 close_subir_fichero();

       });
       $('#file_cancelar').click(function()
       {
       		 close_subir_fichero();

       });
        $('#file_aceptar').click(function()
       {
			$('#subir_fichero').hide();
		});
      // aplicar evento onclick a opcionesavanzadas
    $('.icon-wrench').click(function() 
	{
		if($('.opciones').is(":visible")) 
		{
			$('.opciones').hide();
			$('#avanzadas').removeClass('selected');
			$('#close_avanzada').addClass('avanzadas-close');
		}
		else
		{
			$('.opciones').show();
			$('#avanzadas').addClass('selected');
			$('#close_avanzada').removeClass('avanzadas-close');

		} // end of if
	});
	$('#icon-wrench').click(function() 
	{
		if($('.opciones').is(":visible")) 
		{
			$('.opciones').hide();
			$('#avanzadas').removeClass('selected');
			$('#close_avanzada').addClass('avanzadas-close');
		}
		else
		{
			$('.opciones').show();
			$('#avanzadas').addClass('selected');
			$('#close_avanzada').removeClass('avanzadas-close');

		} // end of if
	});


	$('#close_avanzada').click(function() 
	{
		fn_close_avanzadas();
	});


//	$("#fechaenvio").val( get_Date());	
  	});
  	$('#_open_local').fineUploader({
		request: 
		{
			endpoint: 'scripts/upload.php'
		},
		validation: {
		allowedExtensions: ['csv','txt'],
		sizeLimit: 5120000 // 50 kB = 50 * 1024 bytes
		},
		debug: true
		}).on('error', function(event, id, filename, reason) 
		{

				show_error( "Revise la estructura del fichero."  ,  $('#_open_local') )

 		 })
  		.on('complete', function(event, id, filename, responseJSON )
  		{

  			if( responseJSON.registros )
  			{
  				loaded_file = filename;
            	$('#subir').hide();
            	$('#display_fichero_info').html( '<ul><li>Fichero: ' + filename +'</li><li>Registros: ' + responseJSON.registros +'</li><li>Correctos: ' + responseJSON.correctos +'</li><li>Incorrectos: ' + responseJSON.incorrectos +'</li><li>Duplicados:' + responseJSON.duplicados  + '</li></ul>'  );
                if( responseJSON.duplicados != "0" )
                {
                    $("<label><input id='info_checkbox' type='checkbox'>&nbsp;Eliminar duplicados</label>").appendTo($("#display_fichero_info"));
                } // end of if
            	$('#file_footer').show();
           		$('#display_fichero').show();
           	} // end of if
 	 	}
 	 	
	);

  		//////////////////////////////////////////////////////
  		$('#_open_local_perso').fineUploader({
		request: 
		{
			endpoint: 'scripts/upload_parseo.php'
		},
		validation: {
		allowedExtensions: ['csv','txt'],
		sizeLimit: 51200 // 50 kB = 50 * 1024 bytes
		},
		debug: true
		}).on('error', function(event, id, filename, reason) 
		{

				show_error( "Revise la estructura del fichero."  ,  $('#_open_local_perso') )
 		 })
  		.on('complete', function(event, id, filename, responseJSON )
  		{

  			if( responseJSON.registros )
  			{
  				
  				loaded_file = filename;
            	$('.subir').hide();
            	$('#display_fichero_info').html( '<ul><li>Fichero: ' + filename +'</li><li>Registros: ' + responseJSON.registros +'</li><li>Columnas: ' + responseJSON.columnas +'</li><li>Correctos: ' + responseJSON.correctos +'</li><li>Incorrectos: ' + responseJSON.incorrectos +'</li><li>Duplicados: '+ responseJSON.duplicados + '</li></ul>'  );
                if( responseJSON.duplicados != "0" )
                {
                    $("<label><input id='info_checkbox' type='checkbox'>&nbsp;Eliminar duplicados</label>").appendTo($("#display_fichero_info"));
                } // end of if
            	$('#file_footer').show();
            	$('#display_fichero').show();
            } // end of if
         
 	 	}
 	 	
	);

  		/////////////////////////////////////////////////////

	$( "#tipoenvio" ).mousedown(function() 
	{
		removeError( $(this));
	});
	$( "#tipoenvio" ).change(function() 
	{
		
		load_file();

	});

	// evento click cerrar info
	$('#info_cancelar').click(function()
	{
		close_info();
		return false;
	});
	$('#info_aceptar').click(function()
	{
		 enviar();
		close_info();
	});

	$( "#ficheros" ).click(function() 
	{ 
      		
		$.post( "scripts/existente.php" , { "bbdd" : $('#bbdd').val() , "idcliente": $('#id').val() },
		function( data )
		{
			$('#datos').empty();
			$('#datos').append( data )	
			botonf();
			botondesc();
			window.location.hash="DATOS";

		}); // end of post
	});
	$( "#limpiar" ).click(function() 
	{
         
        $('#texto').val("");
        $('#nom_fichero').val( '' );
        $('#tipoenvio').val( '' );
        $('#fechaenvio').val( '' );
        $('#remitente').val( '' );
        $('#descripcion').val( '' );
        $('#unico').val( '' );
        $('#display_fichero_info').html('');
		$('#file_footer').hide();

        $('#display_fichero').hide();
		return false;
	});
	$( "#datos" ).click(function() 
	{ 

		return false;
	});
	$( "#verpendientes" ).click(function() 
	{ 
			
		$.post( "scripts/verpendientes.php" , { "bbdd" : $('#bbdd').val() , "idcliente": $('#id').val()  },
		function( data )
		{
			$('#datos').empty();
			$('#datos').append( data );
			botonp();
		});
	});
	
	$( "#verhistoricos" ).click(function() 
	{ 

		$.post( "scripts/verhistorico.php" , { "bbdd" : $('#bbdd').val()  , "idcliente": $('#id').val() },
		 function( data )
		  {

			$('#datos').empty();
		  	$('#datos').append(data) ; 
		  	window.location.hash="DATOS";
		  	dar_funcion_fecha();
		  	
		  	plantillas();
		  	$( ".botondesccsv" ).click(function() 
			{   
			        var		aux = $(this).attr('data');
                    $('#idenvio').val( aux.split('#')[0] );
                    document.getElementById('form1').submit();
               		return false;
          });
         		 
		botonh();
			 $( ".botoncsv" ).click(function() 
			{ 
            $("#loader-gif").show('slow');
				$.post( $(this).attr('data') ,
   				function( data ) 
   				{
					var aux = data.split('#');
					$('#' + aux[0] + "_enoperador").html( aux[1] ); 
					$('#' + aux[0] + "_entregados").html( aux[2] ); 
					$('#' + aux[0] + "_fallidos").html( aux[3] );
					var 	label = "<span class='label label-success'>Enviado</span>";
					if( aux[4] == 2 )
					{
						label = "<span class='label label-success'>Enviado</span>";
					}
					else if( aux[4] == 1 )
					{
						label = "<span class='label label-success'>Pendiente</span>";

					}
					else if( aux[4] == 3 )
					{
						label = "<span class='label label-important'>Cancelado</span>";

					}
					else if( aux[4] == 4 )
					{
						label = "<span class='label label-success'>Enviando</span>";

					}
					$('#aceptar_' + aux[0] ).html( label )
            $("#loader-gif" ).hide('slow');
				});
			
				return false;
			 });

		   });
	 });

	// click event to envio
	$( "#envio" ).click(function() 
	{

		if( checkValues() )
		{

	    	if( ( formatear_fechaenvio()  < new Date( get_Date_formateada() ).getTime() ) || $("#fechaenvio").val()== ''  )
        	{
        		var 	sText = 'La fecha seleccionada es anterior a la actual,<br>El envio saldr&aacute; inmediatamente';
        		show_info( sText ,  $(this) );
			}		
        	else
        	{
			    enviar();
        	}
				
		}
		return false;
	});
    // 
	$( "#fechaenvio" ).focus(function() 
	{
	    $( "#fechaenvio" ).val(); 
	});

	$( "input" ).focus(function() 
	{
		removeError( $(this));
	});
	$( "textarea" ).focus(function() 
	{
		removeError( $(this));
	});


	

});
function enviar()
{

	var	url = "scripts/manual.php";
    var     limpiar="";
	
			if(  $('#tipoenvio').val() == 'parseo')
			{
				url = "scripts/parseo.php";
                if($("#info_checkbox").is(':checked'))
                 {  
                       $("#info_ckeckbox").attr('checked', false);  
                        limpiar='true';
                        console.log( "limpiar" + limpiar );

                 } 
			}
			else if(  $('#tipoenvio').val() == 'csv' )
			{
				url = "scripts/telefonos.php";
                if($("#info_checkbox").is(':checked'))
                 {  
                       $("#info_ckeckbox").attr('checked', false);  
                        limpiar='true';
                        console.log( "limpiar" + limpiar );

                 } 
			}


			// comprobar si se fija el limite
			if( ! get_minutos() )
			{
				return false;
			}
			else if( ! get_envios_min( ))
			{
				return false;
			} //

			$.post( url  , { "texto" : encodeURIComponent( $('#texto').val() ) , "unico": $('#unico').val() , "remitente" : $('#remitente').val(), "bbdd" : $('#bbdd').val() , "idcliente": $('#id').val(), 'descripcion': $('#descripcion').val(), 'fechaenvio': $('#fechaenvio').val(), "csv": loaded_file , "minutos" : minutos , "envios_min" : envios_min, 'limpiar': limpiar },
   				function( data ) 
   				{
   					
	   				if( data.substring( 0, 2 ) === "KO")
	   				{
	   					info( data.split('#')[1] );
                     $('#tipoenvio option[value=]').attr("selected", "selected");
	   					$('#datos').empty();
	   				}
	   				else
	   				{
                        $('#tipoenvio option[value=]').attr("selected", "selected");
	   					$('#datos').empty();
	   					$('#datos').append(data)
	   					botonp();
	   				}

		loaded_file = '';
        limpiar="";
		$('#file_footer').hide();
		$('#display_fichero_info').html('');
        $('#display_fichero').hide();
        $('#nom_fichero').val( '' );
			});
}
function checkValues()
{

	var	bCheck =true;
	
	if(  $('#tipoenvio').val() == ''  )
	{
		 setError( $('#tipoenvio') )
		bCheck = false;
	}// end of if
	if(  $('#remitente').val() == '' )
	{
		  setError( $('#remitente') )	
		  bCheck = false;
	}// end of if
	if(  $('#texto').val() == '' )
	{
		  setError( $('#texto') )	
		  bCheck = false;
	}// end of if
	
	if(  $('#tipoenvio').val() == 'manual' && $('#unico').val() == '' )
	{
		  setError( $('#unico') )	
		  bCheck = false;
	}// end of if
	if(  ( $('#tipoenvio').val() != 'manual' &&  $('#tipoenvio').val() != '' )  && loaded_file == '' )
	{
			$( "#INFO" ).dialog({
				height: 180,
				width: 200,
				modal: true,
				open: function() 
				{
					$('#info_label').html( "No ha subido ningun fichero" )
				},
				close: function() 
				{
					$('#info_label').html(  )
				}
								
				}); // end of dialog
		  bCheck = false;
	}// end of if
	if(  $('#tipoenvio').val() == ''  )
	{
		 setError( $('#tipoenvio') )
		  return false;
	}// end of if
	
	return bCheck;
}
function removeError( element )
{	
	  element.parents('.control-group').removeClass('error');;			 
}
function setError( element )
{	
	  element.parents('.control-group').addClass('error');			 
}
function cuenta()
{
    if(  $('#texto').val().length > 160 )
    {
        $('.cuenta').css("background-color", "red" );
    }
    else
    {
        $('.cuenta').css("background-color", "" );
    }
	$('.cuenta').html( $('#texto').val().length )
}
function botonp()
{
	
		$( ".botonp" ).click(function() 
		{

			var envio = $(this).attr('envio');

				$.get( $(this).attr('data'), function( data ) {


				if( data.trim() == "OK" )
				{

					$.post( "scripts/verpendientes.php" , { "bbdd" : $('#bbdd').val() , "idcliente": $('#id').val() },
					function( data )
					{
						$('#datos').empty();
						$('#datos').append(data) ;
						botonp();
					});
				}
						else
						{
							$( "#INFO" ).dialog({
							height: 180,
							width: 200,
							modal: true,
							open: function() 
							{
								$('#info_label').html( data )
							},
							close: function() 
							{
								$('#info_label').html(  )
							}
								
							}); // end of dialog
						} // end of if
				credito();
				});// end of get

					return false;
			});	
}
function botondesc()
{
    $( ".botondesc" ).click(function()
    {
        var aux = $(this).attr('data');
        $('#form2_idenvio').val( aux.split('#')[0] );
        $('#form2_idcliente').val( aux.split('#')[1] );
        document.getElementById('form2').submit();
      return false;
    });
}
function botonf()
{
	dar_funcion_fecha();
	
	$( ".botonf" ).click(function() 
	{

		$('#descripcion').val( $(this).attr('desc') );

        $.post( $(this).attr('data') ,
        function( data ) 
        {
            //alert( data );
		 	if( data.split('#')[0] == 'borrado' )
			{
				//$('#datos').empty();
				//$('#datos').append(data.split('#')[1]);
                $("#" + data.split('#')[1] ).remove(); 
				return false;
			} // end of if
                  loaded_file=  data.split('#')[0]; 
                  $('.subir').hide();
                  $('#display_fichero').show();
                  $('#datos').empty();
                  if( data.split('#')[1] == 1)
                  {
                     //$('#tipoenvio option[value=parseo]').attr("selected", "selected");
                    $( "#tipoenvio" ).val('parseo'); 
                    fn_loaded_file_parseo()
                  }
                  else
                  {
                  	
                    //$('#tipoenvio option[value=csv]').attr("selected", "selected");
                    $( "#tipoenvio" ).val('csv'); 
                       fn_loaded_file();

                  } // end of if
                    
                     
                 });
            return false; 
		});	
}

function credito()
{

	$.post( "scripts/credito.php" , { "bbdd" : $('#bbdd').val() , "idcliente": $('#id').val() },
	 function( data )
	{	
		$('#creditos').html( data );
	}); // end of dialog	
}
function get_Date_formateada()
{
var f = new Date();

var day =  f.getDate() + "";
var month =  f.getMonth() +1  + "";
var year =  f.getFullYear() + "";
var hour =  f.getHours() + "";
var minutes =  f.getMinutes() + "";

	if( day.length == 1)
	{
		day = "0"+day;
	}
	if( month.length == 1 )
	{
		month = "0"+month;
	}
	if( hour.length == 1 )
	{
		hour = "0"+hour;
	}
	if( minutes.length == 1 )
	{
		minutes = "0"+minutes;
	}
	
//return  day + "/" + month + "/" + year + " " + hour + ":" + minutes;
return  month + "/" + day + "/" + year + " " + hour + ":" + minutes;
	
}
function get_Date()
{
var f = new Date();

var day =  f.getDate() + "" ;
var month =  f.getMonth()+1+ "";
var year =  f.getFullYear()+"";
var hour =  f.getHours() + "";
var minutes =  f.getMinutes() + "";

	if( day.length == 1)
	{
		day = "0"+day;
	}
	if( month.length == 1 )
	{
		month = "0"+month;
	}
	if( hour.length == 1 )
	{
		hour = "0"+hour;
	}
	if( minutes.length == 1 )
	{
		minutes = "0"+minutes;
	}
	
return  day + "/" + month + "/" + year + " " + hour + ":" + minutes;
	
}
function PaginacionFicheros( bbdd,idcliente, nropagina, paginas )
{



	if( parseInt( nropagina ) == 0 )
	return false;
	
	if( parseInt( nropagina ) == parseInt( paginas ) + 1 )
	return false;
	


	$.post( "scripts/existente.php" , { "bbdd" : $('#bbdd').val() , "idcliente": $('#id').val() , "pagina": nropagina , "desde" : $('#fichdesde_input').val() , "hasta" : $('#fichhasta_input').val()  },
	function( data )
	{
		$('#datos').empty();
		$('#datos').append( data )

		var corrector = 0 ;	
		
		// mostrar 5 p�gina anterirores 
			var inicio = parseInt( nropagina ) - 5;
			if( inicio < 0 )
			{	
				corrector = inicio*(-1); 
				inicio = 0;	
			}
			var fin = parseInt( nropagina ) + 6 ;
			fin = fin + corrector;

			if( fin > paginas )
			{
				corrector = fin - paginas;
				fin = paginas;	
			}		
			
			inicio = inicio - corrector;	
			for( i = inicio; i < fin ; i++  )
			{
				$('#pf_'+ i ).show();	
			}

		botonf();
			botondesc();
	});
		
	            return false; 
}
function Paginacion ( bbdd,idcliente,nropagina, paginas )
{	


	if( parseInt( nropagina ) == 0 )
	return false;
	
	if( parseInt( nropagina ) == parseInt( paginas ) + 1 )
	return false;
	

		$.post( "scripts/verhistorico.php" , { "bbdd" : $('#bbdd').val(), "idcliente":idcliente , "pagina": nropagina , "desde" : $('#fichdesde_input').val() , "hasta" : $('#fichhasta_input').val()   },
		 function( data )
		  {

			$('#datos').empty();
		  	$('#datos').append(data) ; 
		  	dar_funcion_fecha();
		  		var corrector = 0 ;	
		
		// mostrar 5 p�gina anterirores 
			var inicio = parseInt( nropagina ) - 5;
			if( inicio < 0 )
			{	
				corrector = inicio*(-1); 
				inicio = 0;	
			}
			var fin = parseInt( nropagina ) + 6 ;
			fin = fin + corrector;

			if( fin > paginas )
			{
				corrector = fin - paginas;
				fin = paginas;	
			}		
			
			inicio = inicio - corrector;	
			for( i = inicio; i < fin ; i++  )
			{
				$('#ph_'+ i ).show();	
			}
		  	
		  	botonh();
		  	plantillas();
			 $( ".botoncsv" ).click(function() 
			{ 
            $(".loader-gif" ).show();
				$.post( $(this).attr('data') ,
   				function( data ) 
   				{
					var aux = data.split('#');
					$('#' + aux[0] + "_enoperador").html( aux[1] ); 
					$('#' + aux[0] + "_entregados").html( aux[2] ); 
					$('#' + aux[0] + "_fallidos").html( aux[3] ); 
					var 	label = "<span class='label label-success'>Enviado</span>";
					if( aux[4] == 2 )
					{
						label = "<span class='label label-success'>Enviado</span>";
					}
					else if( aux[4] == 1 )
					{
						label = "<span class='label label-success'>Pendiente</span>";

					}
					else if( aux[4] == 3 )
					{
						label = "<span class='label label-important'>Cancelado</span>";

					}
					else if( aux[4] == 4 )
					{
						label = "<span class='label label-success'>Enviando</span>";

					}
					$('#aceptar_' + aux[0] ).html( label )
            $(".loader-gif" ).hide();
				});
			
				return false;
			 });
		  	$( ".botondesccsv" ).click(function() 
			{   
			    var aux = $(this).attr('data');
                $('#idenvio').val( aux.split('#')[0] );
                document.getElementById('form1').submit();
               	return false;
          });
            // 
		   });
}
function formatear_fechaenvio()
{
   var  aux = $('#fechaenvio').val().split( ' ' ); 
    var aux1 =  aux[0].split('/');
    aux1 = aux1[1] + "/" + aux1[0] + "/" + aux1[2] + " " + aux[1];

    return new Date( aux1 ).getTime()
}
function botonh()
{
	
	$( ".botonh" ).click(function() 
	{ 
			var envio = $(this).attr('envio');

			$.get( $(this).attr('data'), function( data ) {

				if( data.trim() == "OK" )
				{

					$.post( "scripts/verhistorico.php" , { "bbdd" : $('#bbdd').val() ,  "idcliente": $('#id').val() },
					function( data )
					{
						$('#datos').empty();
						$('#datos').append(data) ;
						dar_funcion_fecha();


						botonh();
					});
				}
				else
				{
					$( "#INFO" ).dialog({
					height: 180,
					width: 200,
					modal: true,
					open: function() 
					{
						$('#info_label').html( data )
					},
					close: function() 
					{
						$('#info_label').html(  )
					}
								
					}); // end of dialog
				} // end of if
				credito();
				});// end of get

					return false;
			 });	
}

function dar_funcion_fecha()
{

  			$('#fichdesde').datetimepicker({
  				 pickTime: false
     		});	
     			$('#fichhasta').datetimepicker({
  				 pickTime: false
     		});	
}
function plantillas()
{

		$( ".texto_holder" ).click(function() 
		{ 		
			$('#texto').val( $(this).attr('title') );
			$('#caracteres').val( $('#texto').val().length )
		});
		$( ".remitente_holder" ).click(function() 
		{ 		
			$('#remitente').val( $(this).html() );
		});	
}
function setMinutes( sMinutes )
{

	var	aAux = sMinutes.split(':');
	return  ( parseInt( aAux[0] )* 60 ) + parseInt( aAux[1] );

}
function show_info( sText , element )
{
	$('#info_label').html( sText );
	$('#INPUT').css( "left", element.position().left + 'px');
	$('#INPUT').css( "top", element.position().top + 'px');
	$('#INFO').show(); 
}
function close_info()
{
	
	$('#INFO').hide(); 
	$('#info_label').html();

}
function close_subir_fichero()
{
	$('#subir_fichero').hide();
	//$('#tipoenvio').val( '' );
	console.log('poraqui')
	$('#tipoenvio option[value=]').attr("selected", "selected");

	loaded_file ='';

}

function load_file()
{
	removeError( $(this));
        $('#display_fichero').hide();
        $('#file_footer').hide();

        $('#nom_fichero').val( '' );
		if( $( "#tipoenvio" ).val() != 'manual' && $( "#tipoenvio" ).val() != '' )
		{
			// show dialog box
			$('#subir_fichero').addClass('selected');
			$('#subir_fichero').css( "left", $('#remitente').position().left + 'px');
			$('#subir_fichero').css( "top", $('#remitente').position().top + 'px');


			if( $( "#tipoenvio" ).val() == 'parseo'  )
          	{

          		$('#_open_local').hide();
				$('#_open_local_perso').show();
          	}
          	else
          	{
				$('#_open_local').show();
				$('#_open_local_perso').hide();

			} // end of if

			$('#subir_fichero').show();
  			$('.subir').show();
  		} // end of if
  		else
  		{
  			loaded_file = '';
  			$('.subir').hide();
            $('#subir_fichero').hide();
            $('#nom_fichero').val( '' );
    		//$('#subir_fichero').dialog('close');

  		} // end of if
        
        if( $( "#tipoenvio" ).val() == 'parseo'  )
        {
              $('#texto').attr("maxlength","300");
        }
} // end of load file

function fn_loaded_file()
{

	var 	url = "scripts/loaded.php";
$.getJSON( url , { "file" :  loaded_file } )
  .done(function( responseJSON ) {
		// show dialog box
		$('#subir_fichero').addClass('selected');
		$('#subir_fichero').css( "left", $('#remitente').position().left + 'px');
		$('#subir_fichero').css( "top", $('#remitente').position().top + 'px');

		$('#subir_fichero').show();
		$('#_open_local').hide();
		$('#_open_local_perso').hide();
        $('#display_fichero_info').html( '<ul><li>Fichero: ' + loaded_file +'</li><li>Registros: ' + responseJSON.registros +'</li><li>Correctos: ' + responseJSON.correctos +'</li><li>Incorrectos: ' + responseJSON.incorrectos +'</li></ul>'  );
        $('#file_footer').show();
        $('#display_fichero').show();
						
	});


} // end of fn_loaded_file

function fn_loaded_file_parseo()
{

	var 	url = "scripts/loaded_parseo.php";
$.getJSON( url , { "file" :  loaded_file } )
  .done(function( responseJSON ) {
		// show dialog box
		$('#subir_fichero').addClass('selected');
		$('#subir_fichero').css( "left", $('#remitente').position().left + 'px');
		$('#subir_fichero').css( "top", $('#remitente').position().top + 'px');

		$('#subir_fichero').show();
		$('#_open_local').hide();
		$('#_open_local_perso').hide();
        $('#display_fichero_info').html( '<ul><li>Fichero: ' + loaded_file +'</li><li>Registros: ' + responseJSON.registros +'</li><li>Columnas: ' + responseJSON.columnas +'</li><li>Correctos: ' + responseJSON.correctos +'</li><li>Incorrectos: ' + responseJSON.incorrectos +'</li></ul>'  );
        $('#file_footer').show();
        $('#display_fichero').show();
						
	});


} // end of fn_loaded_file

function fn_close_avanzadas()
{
		removeError( $('#chk_distribuido') );
    	removeError( $('#chk_por_minuto') );
    	//desmarcamos checkboxs y desabilitamos la caja de texto
    

	//desmarcamos checkboxs y desabilitamos la caja de texto
	$('#chk_distribuido').attr('checked', false);
	$('#chk_por_minuto').attr('checked', false);

	$('#hourpicker2').val( '1' );
	$('#timepicker2').val( '00:01' );
	$('.opciones').hide();
	$('#avanzadas').removeClass('selected');
	$('#close_avanzada').addClass('avanzadas-close');
} // end of close_avanzadas

function get_minutos()
{
	
        if($('#chk_distribuido').is(":checked")) 
        {
        	if( $('#timepicker2').val() == '' )
        	{
        		setError( $('#timepicker2') )
        		return false;
        	}
        	minutos = setMinutes( $('#timepicker2').val() );
        }
        else
        {
        	minutos = "";
        }
    return true;
} // end of get_limit()

function get_envios_min()
{
	  if( $('#chk_por_minuto').is(":checked"))  
        {
        	if( $('#hourpicker2').val() == '' || !parseInt( $('#hourpicker2').val() ))
        	{
        		setError( $('#hourpicker2') )
        		return false;
        	}
        	envios_min =  $('#hourpicker2').val();
        } // end of if
        else
        {
        	envios_min = "";
        }
    return true;
}
function show_error( sText , element )
{
	$('#error_label').html( sText );
	$('#ERRORINPUT').css( "left", element.position().left + 'px');
	$('#ERRORINPUT').css( "top", element.position().top + 'px');
	$('#ERRORINFO').show(); 
}

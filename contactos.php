<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Expires" content="Mon, 26 Jul 1997 05:00:00 GMT">
		    <meta http-equiv="Last-Modified" content="Sun, 25 Jul 2004 16:12:09 GMT">
				<meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
					    <meta http-equiv="Pragma" content="nocache">
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="css/bootstrap-responsive.min.css">
	<link type="text/css" rel="stylesheet" href="css/dialerisk.css">
		<link type="text/css" rel="stylesheet" href="css/gestor.css">


	


	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script src="js/jquery.fineuploader-3.0.js" language="JavaScript"></script>
	<SCRIPT src="js/modal.js" language="JavaScript"></script>  

	<SCRIPT src="js/gestor.js" language="JavaScript"></script>  

	<link type="text/css" rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
	<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	
	<link type="text/css" rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
	<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	
	<style type="text/css">
		
		
	
	</style>
	<title>Gesti&oacute;n de Env&iacute;os</title>
</head>

<body>
<form id="form2" name="formulario" action='scripts/descargarcsv.php' method='post'  >
<input type='hidden' name='form2_idenvio' id='form2_idenvio' value ="">
<input type='hidden' name='form2_idcliente' id='form2_idcliente' value ="">
</form>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
				<a class="logo"><img width='13%' id='logo' src="img/<?php echo $_REQUEST['logo']; ?>"></a> 

			<div class="container">
				<button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button">
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span> 
					<span class="icon-bar"></span>
				</button>
				<a class="brand" href="#"></a>
				<div class="nav-collapse collapse">
					<ul class="nav">
						
							<li><a href="#" ID ="verpendientes" title='Muestra pendientes de envio' >Programados</a></li>
							<li><a href="#" ID="verhistoricos"  title='Historico de envíos realizados'>Hist&oacute;ricos</a></li>
							<li><a href="#" ID="ficheros"  title='Muestra fichero cargados en envíos previos'>Ficheros</a></li>
							<li><a href="#" ID="manual"  title='Descarga Manual'>Manual</a></li>

					</ul>
					<ul class="nav pull-right">
						<li class="pull-right"><a href="index.html"><i class="icon-off"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div id="statsHolder" class="container">
		<div class="row-fluid">
			<div class="span12">
			
				

				<div id="main-container-nav-tabs" class="tab-content">
		
					<div id="users" class="tab-pane fade in active">
						<div class="span12">
							<div id="legend">
								<legend>Datos de Env&iacute;o</legend>
								
							</div>
							
							<form class="form-inline" id="form1" name="formulario" action='scripts/generadorcsv.php' method='post'>
								
								<div class="span6">
							<input type='hidden' name='upload_url' id='upload_url' value ="scripts/upload.php">

							<input type='hidden' name='bbdd' id='bbdd' value ="<?php echo trim($_REQUEST['bbdd']) ?>">
								<input type='hidden' name='id' id='id' value ="<?php echo $_REQUEST['id'] ?>">
								<input type='hidden' name='idenvio' id='idenvio' value ="">
									<div class="control-group">

										<label class="control-label" for="selectmultiple" style="padding-top: 10px; width: 150px; text-align: right">Tipo de Env&iacute;o</label>
										<select ID='tipoenvio' name="tipoenvio" title='Seleccione tipo de envío'   style="margin-left: 30px;">
											<option value="" >Seleccione Tipo Envio</option>
											<option value="manual" >Manual</option>
											<option value="parseo" >Csv Personalizado</option>
											<option value="csv" >Csv Solo telefonos</option>
										</select>
									</div>
									<!-- Text input-->
									<div class="control-group">
										<label class="control-label" or="textinput" style="padding-top: 10px; width: 150px; text-align: right">Remitente:</label>
										<input  type="text" ID="remitente" placeholder="M&aacute;ximo 11 caracteres" value="" maxlength=11 title='Remitente de envio. Max 11 caracteres' style="margin-left: 30px;">

									</div>
									
									<div class="control-group">
										<label  class="control-label" for="textinput" style="padding-top: -10px; width: 150px; text-align: right">Texto a Enviar:</label>
                <textarea rows="4"  ID='texto' maxlength="160" placeholder="Texto a enviar, m&aacute;ximo 160 caracteres"  onKeyDown="cuenta()" onKeyUp="cuenta()" title='texto a enviar, máximo 160 caracteres' class="input-block-level" ></textarea>
                <label class='cuenta' ></label>
									</div>
									<div class="control-group">
										<label class="control-label" for="textinput" style="padding-top: 10px; width: 150px; text-align: right">Control:</label>
                <textarea rows="1"  id='unico' placeholder="N&uacute;meros de control: 666666666,666666662,6..."  title='' class="input-block-level" ></textarea>
									</div>
									
									<!-- Select Multiple -->
									
								</div>
								
								<div class="span6">		
									<div class='date_container'>
										<div class="control-group">

											<div id="fechaenvio_div" >
												<label class="control-label" for="initContactDate" style="padding-top: 10px; width: 100px; text-align: right">Fecha Env&iacute;o:</label>
										
											 <div id="datetimepicker1" class="input-append date">
    <input data-format="dd/MM/yyyy hh:mm" type="text" id='fechaenvio' class='input-medium'></input>
    <span class="add-on">
      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
      </i>
    </span>
  </div>
											</div>
										</div>
								
	<div id="avanzadas" title="Opciones avanzadas" >

										<!-- Multiple Checkboxes -->
		  <label class="control-label" ID='opcionesavanzadas' for="checkboxes"><h5><i class="icon-wrench"></i><label ID="icon-wrench">&nbsp;Programaci&oacute;n Avanzada&nbsp;&nbsp;</label><i title="Cancelar Programaci&oacute;n Avanzada" class="icon-off avanzadas-close" id="close_avanzada"></i></label>
  	
  		<div class='opciones'>
  			<div class="control-group">

  	   			<label ID='checkbox-inline' class="checkbox inline" for="checkboxes-0">
    	  			<input type="checkbox" name="checkboxes" id='chk_distribuido' value="distribuido" >Periodo de tiempo:
    			</label>
     	 		<label class='input-append bootstrap-timepicker' for="radios-0">
					<input id="timepicker2" type="text" class="input-small" data-format="hh:mm" value='00:01' style="margin-left: 5%;" title = 'Distribuir envío en un intervalo' >
            		<span class="add-on">
               			 <i class="icon-time"></i>
            		</span>
         		</label>
         	</div>
   		</div>
    <div class='opciones'>
    	<div class="control-group">
			<label ID='checkbox-inline' class="checkbox inline" for="checkboxes-0">
    			  <input type="checkbox" name="checkboxes" id='chk_por_minuto' value="porminuto" > Distribuir el envio en:
    		</label>
   		
			<input  type="text" id='hourpicker2' title='Envios por minuto' value='1' style="margin-left: 2%;max-width:10%;" >
			 <label for="textinput" style="padding-top: 10px;text-align: right"> x min</label>
	    </div>
	</div>

</div>
	
								
									</div>
									<div  class="rigth_side" >
										<section id="categories-2" class="widget widget_categories">
											<div id='creditos' >
											</div>
										</section>
									</div>
				
							</div>
								<div class="span11" style="padding-top: 10px; text-align: center">	
									<button id="envio" style="width:150px; margin: auto;" name="singlebutton" class="btn btn-primary">Aceptar</button>
								</div>
								
							</form>
							
							<div id="tableResults" class="span11" style="margin-top: 20px;">
								<DIV ID='datos'>
								</div>
								<a name="DATOS"></a>

							</div>
	<div ID='loader-gif' >
    <img src='img/ajax-loader.gif' >
    </div>

						</div>
						<div class="span11">
							<div id="table-paginated" class ='center' >
            <div class="pagination" id="thePagination"></div>
           
</div>
						</div>
					</div>
					
				</div><!-- tab-content -->
			</div> <!-- span12 -->
		</div> <!-- row-fluid -->
		  
		
				
	</div>

	</DIV>

	<DIV id="INFO_OPCIONES" title="Info">
			<LABEL ID='info_label_opciones'></LABEL>
	</DIV>

<DIV id="subir_fichero" title="Gestión ficheros" >
		<div style="padding: 10px 0px; width: 100%; height: 19px;"><div style="float: left; padding-left: 10px;"><h5 style="margin: 0px;"><i class="icon-file"></i>&nbsp;Subir Fichero</h5></div>
        <ul class="nav pull-right">
                        <li class="divider-vertical"></li>
                        <li class="pull-right"><i title="Cerrar gestor" class="icon-off" id="close_subir"></i>&nbsp;</li>
                    </ul></div>
					<hr class='separador_info' >
	
		<DIV  ID="display_fichero" >
			<DIV  ID="display_fichero_info" ></DIV>
		</DIV>
		<DIV class="option_container">

		 	<input class="input_fichero" type="text"  placeholder='Descripcion Fichero' ID="descripcion" title='descripción del fichero que se desea cargar'/>
		</DIV>	
		<div id='file_footer' class="modal-footer">
        <button type="button" id='file_cancelar' class="btn btn-default" >Cancelar</button>
        <button type="button" id='file_aceptar' class="btn btn-primary">Aceptar</button>
      </div>
		<DIV  ID="_open_local" class='load_btn'>
		</DIV>
		<DIV  ID="_open_local_perso" class='load_btn'>
		</DIV>
</DIV>
<DIV id="INFO" title="Info">
		<h5><i class="icon-exclamation-sign"></i>&nbsp;Info</label>	
		<ul class="nav pull-right">
						<li class="divider-vertical"></li>
						<li class="pull-right"><i id='close_info' class="icon-off" title='Cerrar gestor'></i>&nbsp;</li>
					</ul>
					<hr>
		<LABEL ID='info_label'></LABEL>
		<hr>
<div class="modal-footer">
        <button type="button" id='info_cancelar' class="btn btn-default" >Cancelar</button>
        <button type="button" id='info_aceptar' class="btn btn-primary">Aceptar</button>
      </div>
</DIV>
<DIV id="ERRORINFO" title="Info">
		<h5><i class="icon-exclamation-sign"></i>&nbsp;Info</label>	
		<ul class="nav pull-right">
						<li class="divider-vertical"></li>
						<li class="pull-right"><i id='close_error' class="icon-off" title='Cerrar gestor'></i>&nbsp;</li>
					</ul>
					<hr>
		<LABEL ID='error_label'></LABEL>
		<hr>
		<div class="modal-footer">
        <button type="button" id='error_aceptar' class="btn btn-primary">Aceptar</button>
      </div>
</DIV>
</body>
</html>

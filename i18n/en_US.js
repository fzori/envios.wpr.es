var label = {
		"common": {
			"campaign": {
				"status":{
					"NOT_READY": "NOT READY",
					"READY": "READY",
					"RUNNING": "EXECUTING",
					"PAUSE": "PAUSED",
					"FINISHED": "FINISHED"
				}
			}
		}
	}

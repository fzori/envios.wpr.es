<?php
session_start();
	//*********************
	//* Variable declaration 
	//**********************
	$aAllowedExt;
	// max file size in bytes
	$iSizeLimit;
	// loader class object
	$oLoader;

	// respuesta
	$aResp = array();
	$areport = array();
	
	
	//*************************
	//* Variable initialization 
	//*************************
	
	
	//$result = $oLoader->handleUpload('tmp/files');
	if( isset( $_REQUEST['file'] ) )
	{
		
		$areport =ver_fichero(  $_REQUEST['file']  );
		$aResp = array('success'=>$areport[4] ,'registros'=> $areport[0] ,'correctos'=> $areport[1] , 'incorrectos' => $areport[2], 'columnas' => $areport[3]  );

	}
	echo htmlspecialchars( json_encode( $aResp ) , ENT_NOQUOTES);

/******************************************/
function ver_fichero( $myFile  )
{

	// respuesta
	$aResp = array();
	$aResp[0] = 0; 
	$aResp[1] = 0; 
	$aResp[2] = 0; 
	$aResp[3] = 0; 
	$aResp[4] = true;
	$fila = 0;


	if (($gestor = fopen("tmp/files/" . $myFile , "r")) !== FALSE) 
	{
		while (($datos = fgetcsv( $gestor , 1000, ";" )) !== FALSE) {
        $aResp[3] = count($datos);
       		if( $fila == 0 )
        	{
        		array_walk( $datos , 'to_upper' );
		       	
        		$iTelefono = ( array_search( "TELEFONO" ,  $datos  ) ) ;
        		logger( "parseo entro " ,   $iTelefono . " " . "TELEFONO" );
        			if( trim( $iTelefono ) == ''  )
        			{
				
						    fclose( $gestor );
							unlink( "tmp/files/" . $_FILE );
        					echo "KO#No hemos detectado la columna Telefono en el fichero";
        					exit(0);	
        			} // en dof if
        	} // end of if
        	else
        	{
        		logger('info xx' , $aResp[1] . " " .  $datos[ $iTelefono ] );
        	
    	 		if( check_number( trim(  $datos[ $iTelefono ] )  ) )
    	 		{
    	 			$aResp[1]++; 
    	 		}
    	 		else
    	 		{
    	 			$aResp[2]++;	
    	 		} // end of if
    	 		$aResp[0]++;

        	} // end of if
    	 $fila++;

		} // end of while
	} // end od if
	fclose( $gestor );
	return $aResp;
}
function check_number( $number )
{
	//351933136768

		if( !is_numeric( $number ))
		{
			logger('warn', $number . " es Incorrecto ");
			return false;
		}
		if( strlen( $number ) < 7  )
		{
			logger('info', $number . " es correcto ");
			return false;
		}
		if( strlen( $number ) == 9 && ( startsWith( $number , '6' ) || startsWith( $number , '7' ) ) )
		{
			logger('info', $number . " es correcto ");
			return true;
		}
		if( strlen( $number ) == 11 && ( startsWith( $number , '346' ) || startsWith( $number , '347' ) ) )
		{
			logger('info', $number . " es correcto ");
			return true;
		}
		if( strlen( $number ) == 13 && ( startsWith( $number , '00346' ) || startsWith( $number , '00347' ) ) )
		{
			logger('info', $number . " es correcto ");
			return true;
		}

		// comprobamos prefijos de oreos paises
	for($i=0; $i < count($_SESSION['prefijos']) ; $i++) 
	{ 
		logger('Info' ,$number  . " "  . $_SESSION['prefijos'][ $i] );
		if( startsWith( $number , $_SESSION['prefijos'][ $i] ) )
		{
			return true;
		}

	} // end of for
		
		logger('warn', $number . " prefijo no reconocido ");

		return false;
		
}
function logger( $nivel , $texto )
{
$ddf = fopen('received.log','a');
fwrite($ddf,"[".date("r")."] $nivel $texto \n");
fclose($ddf);
}
function startsWith($haystack, $needle)
{
    return !strncmp($haystack, $needle, strlen($needle));
}
function to_upper (&$datos, $key )
{
    $datos = strtoupper( $datos );
}
?>

<?php
session_start();

	require('loader.class.php');
	//*********************
	//* Variable declaration 
	//**********************
	$aAllowedExt;
	// max file size in bytes
	$iSizeLimit;
	// loader class object
	$oLoader;

	// respuesta
	$aResp = array();
	$areport = array();
	
	
	//*************************
	//* Variable initialization 
	//*************************
	// list of valid extensions, ex. array("jpeg", "xml", "bmp")
	$aAllowedExt = array("csv","txt");
	// max file size in bytes
	$iSizeLimit = 100 * 1024 * 1024;
	
	$oLoader = new qqFileUploader( $aAllowedExt, $iSizeLimit );

	// Call handleUpload() with the name of the folder
	//$result = $oLoader->handleUpload('tmp/files');
	if( $oLoader->handleUpload('tmp/files') )
	{
		
		$areport =ver_fichero(  $oLoader->getUploadName() );
		$aResp = array('success'=>true,'registros'=> $areport[0] ,'correctos'=> $areport[1] , 'incorrectos' => $areport[2] , 'duplicados'=> $areports[3] , 'duplicados'=>$areport[3]);

	}
	echo htmlspecialchars( json_encode( $aResp ) , ENT_NOQUOTES);

/******************************************/
function ver_fichero( $myFile  )
{
	// respuesta
	$aResp = array();
	$aUnicos = array();
	$aResp[0] = 0; 
	$aResp[1] = 0; 
	$aResp[2] = 0; 
	$aResp[3] = 0; 

	$file = fopen("tmp/files/" . $myFile , "r") or exit("Error abriendo fichero!");
	while($linea = fgets($file)) 
	{
    	if (feof($file))
    	{
    		break;	
    	}
    	else
    	 {
    	 	$aResp[0]++;
    	 	if( check_number( trim( $linea )  ) )
    	 	{
    	 		$aResp[1]++; 
	            $aUnicos[ $linea ] = "OK";
    	 	}
    	 	else
    	 	{
    	 		$aResp[2]++;	
    	 	}
    	 } 
    		
	} // end of while
	fclose($file);
   	$aResp[3] = $aResp[1] -  count( $aUnicos );	
    logger("info duplicados " ,  $aResp[3] );
	return $aResp;
}
function check_number( $number )
{
	//351933136768
		if( !is_numeric( $number ))
		{
			logger('warn', $number . " es Incorrecto ");
			return false;
		}
		if( strlen( $number ) < 7  )
		{
			logger('info', $number . " es correcto ");
			return false;
		}
		if( strlen( $number ) == 9 && ( startsWith( $number , '6' ) || startsWith( $number , '7' ) ) )
		{
			logger('info', $number . " es correcto ");
			return true;
		}
		if( strlen( $number ) == 11 && ( startsWith( $number , '346' ) || startsWith( $number , '347' ) ) )
		{
			logger('info', $number . " es correcto ");
			return true;
		}
		if( strlen( $number ) == 13 && ( startsWith( $number , '00346' ) || startsWith( $number , '00347' ) ) )
		{
			logger('info', $number . " es correcto ");
			return true;
		}

		// comprobamos prefijos de oreos paises
	for($i=0; $i < count($_SESSION['prefijos']) ; $i++) 
	{ 
		logger('Info' ,$number  . " "  . $_SESSION['prefijos'][ $i] );
		if( startsWith( $number , $_SESSION['prefijos'][ $i] ) )
		{
			return true;
		}

	} // end of for
		
		logger('warn', $number . " es Incorrecto ");

		return false;
		
}
function logger( $nivel , $texto )
{
$ddf = fopen('received.log','a');
fwrite($ddf,"[".date("r")."] $nivel $texto \n");
fclose($ddf);
}
function startsWith($haystack, $needle)
{
    return !strncmp($haystack, $needle, strlen($needle));
}
?>
